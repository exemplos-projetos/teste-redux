import { combineReducers, createStore } from 'redux';


// export let testeSoma = () => ({
//   type: 'teste',
// })

var valor = 1

let funcaoAtualizacao = () => {
  return valor++;
}


export const closeGeod = () => ({
  type: funcaoAtualizacao(),
});


export const geod = (state ={}, action) => {
  console.log(action)
  return action.type
  
};


export const reducers = combineReducers({
  geod,
});

// store.js
export function configureStore(initialState = {}) {
  const store = createStore(reducers, initialState);
  return store;
}

export const store = configureStore();
