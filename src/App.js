import React from 'react';

import TesteComponent from './TesteComponent'

import { connect } from 'react-redux';
import { closeGeod } from './redux';


// App.js
export class App extends React.Component {
  render() {
    return (
      <div>
        <h1>'Teste Redux!'</h1>
        {this.props.geod}

        <TesteComponent />

          <button
            onClick={this.props.closeGeod}
          >
            sOMA
          </button>

      </div>
    );
  }
}

// AppContainer.js
const mapStateToProps = state => ({
  geod: state.geod,
});


const mapDispatchToProps = {
  closeGeod
};

const AppContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(App);



export default AppContainer;
