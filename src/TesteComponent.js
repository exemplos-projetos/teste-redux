import React from 'react';
import { connect } from 'react-redux';

export class TesteComponent extends React.Component {
  render() {
    return (
      <div>
        <br />
          Teste COmpontent: {this.props.geod}
        <br />
      </div>
    );
  }
}


// AppContainer.js
const mapStateToProps = state => ({
    geod: state.geod,
});
  
    
const TesteComponentContainer = connect(
    mapStateToProps
)(TesteComponent);
  


export default TesteComponentContainer;

